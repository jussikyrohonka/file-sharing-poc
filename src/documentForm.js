'use strict';

const express = require('express');
const util = require('util');
const request = require('request');
const ipLib = require('ip');
const ipaddrLib = require('ipaddr.js');

const upload = require('./lib/upload');
const oauth2 = require('./lib/oauth2');
const config = require('./config');

const CLOUD_BUCKET = config.get('CLOUD_BUCKET');
var storage = require('@google-cloud/storage')();
var bucket = storage.bucket(CLOUD_BUCKET);

const document = require('./db/document');
const version = require('./db/version');
const action = require('./db/action');

const router = express.Router();

// Use the oauth middleware to automatically get the user's profile
// information and expose login/logout URLs to templates.
router.use(oauth2.template);

// Set Content-Type for all responses for these routes
router.use((req, res, next) => {
    res.set('Content-Type', 'text/html');
    next();
});

/**
 * GET /document
 *
 * Display a page of books (up to ten at a time).
 */
router.get('/', (req, res, next) => {
    // redirect to /home
    res.redirect('/home');
});


/**
 * GET /books/add
 *
 * Display a form for creating a document.
 */
router.get('/add', (req, res) => {
    res.render('user/upload.pug', {
	document: {}, // empty object for the form page
	action: 'Add'
    });
});

/**
 * POST /books/add
 *
 * Create a book.
 */
// [START add]
router.post(
    '/add',
    upload.multer.single('file'),
    upload.sendUploadToGCS,
    (req, res, next) => {
	// File should have been added
	if (!req.file || !req.file.cloudStorageObject) {
	    console.log('Warning. No file uploaded for document add');
	    res.redirect('/home');
	    return;
	}
	//const cloudStorageUrl = req.file.cloudStoragePublicUrl;
	const cloudStorageObject = req.file.cloudStorageObject;
	
	const body = req.body;
	const user = req.user;
	const userId = user.id;
	
	const data = makeDocumentData(body, user);
	
	// Save the document to the database.
	document.create(data, (err, savedData) => {
	    console.log(util.format('document.create() Done()'));
	    if (err) {
		next(err);
		return;
	    }
	    var documentId = savedData.id;
	    const versionData = makeFirstVersionData(
		savedData, userId, cloudStorageObject);
	    
	    saveVersion(versionData, (err, savedVersionData) => {
		if (err) {
		    next(err);
		    return;
		}
		res.redirect(`${req.baseUrl}/${documentId}`);
		//res.redirect(`${req.baseUrl}/${savedData.id}`);
	    });
	});
	
	logUploadAction(req, entity, () => {
	    if (err) {
		console.warn('Failed to log upload action: ' + err);
	    }
	});
    }
);

function makeDocumentData(object, user) {
    // User should be logged in
    object.createdBy = user.displayName;
    object.createdById = user.id;
    object.group = user.group;
    object.currentVersion = 1;
    object.created = (new Date()).toISOString();
    return object;
}

function makeFirstVersionData(documentData, userId, cloudStorageObject) {
    const versionData = {};
    versionData.documentId = documentData.id;
    versionData.created = documentData.created;
    versionData.version = 1;
    versionData.object = cloudStorageObject;
    versionData.userId = userId;
    return versionData;
}

function saveVersion(versionData, cb) {
    console.log(util.format('saveVersion()'));
    
    version.create(versionData, (err, savedVersionData) => {
	console.log(util.format('version.create() Done()'));
	cb(err, savedVersionData);
    });
}
// [END add]

/**
 * GET /books/:id/edit
 *
 * Display a book for editing.
 */
router.get('/:book/edit', (req, res, next) => {
    document.read(req.params.document, (err, entity) => {
	if (err) {
	    next(err);
	    return;
	}
	res.render('user/upload.pug', {
	    document: entity,
	    action: 'Edit'
	});
    });
});

/**
 * POST /books/:id/edit
 *
 * Update a book.
 */
router.post(
    '/:document/edit',
    upload.multer.single('file'),
    upload.sendUploadToGCS,
    (req, res, next) => {
	const data = req.body;

	// Was an image uploaded? If so, we'll use its public URL
	// in cloud storage.
	if (req.file && req.file.cloudStoragePublicUrl) {
	    req.body.imageUrl = req.file.cloudStoragePublicUrl;
	}

	document.update(req.params.book, data, (err, savedData) => {
	    if (err) {
		next(err);
		return;
	    }
	    res.redirect(`${req.baseUrl}/${savedData.id}`);
	});
    }
);

/**
 * POST /document/version
 *
 * Add a version
 */
router.post(
    '/version',
    upload.multer.single('file'),
    upload.sendUploadToGCS,
    (req, res, next) => {
	const body = req.body;
	console.log(body);
	var documentId = body.documentid;
	//var userId1 = req.session.profile.id;
	var userId = body.userid;
	console.log(util.format('New version for %s by %s',
				documentId, userId));
	
	if (!req.file || !req.file.cloudStorageObject) {
	    console.log('Warning. No file uploaded for version add');
	    res.redirect('/home');
	    return;
	}
	const cloudStorageObject  = req.file.cloudStorageObject;
	
	document.read(documentId, (err, entity) => {
	    console.log('Read document data');
	    if (err) {
		next(err);
		return;
	    }
	    
	    const accessAllowed = isAccessAllowed(req, entity);
	    if (!accessAllowed) {
		res.redirect('/home');
		return;
	    }
	    
	    // Bump up the current version number
	    const gcsName = entity.gcsName
	    const newVersion = entity.currentVersion + 1;
	    entity.currentVersion = newVersion;
	    
	    document.update(documentId, entity, (err, data) => {
		console.log('Updated document version to: ' + data.currentVersion);
		if (err) {
		    next(err);
		    return;
		}
		
		// Build the new version object
		const versionData = makeNewVersionData(
		    documentId, newVersion, userId, cloudStorageObject);
		
		version.create(versionData, (err, savedVersionData) => {
		    console.log(util.format('version.create() Done()'));
		    if (err) {
			next(err);
			return;
		    }
		    res.redirect(`${req.baseUrl}/${documentId}`);
		});
	    });
	});
	
	logVersionAction(req, entity, (err) => {
	    if (err) {
		console.warn('Failed to log upload action: ' + err);
	    }
	});
    });

function makeNewVersionData(documentId, newVersion, userId, cloudStorageObject) {
    const versionData = {};
    versionData.documentId = documentId;
    versionData.created = (new Date()).toISOString();
    versionData.version = newVersion;
    versionData.object = cloudStorageObject;
    versionData.userId = userId;
    return versionData;
}


/**
 * GET /document/:id
 *
 * Display a document.
 */
router.get('/:documentId', (req, res, next) => {
    var documentId = req.params.documentId;
    console.log(util.format('Viewing document %s', documentId));
    document.read(documentId, (err, entity) => {
	if (err) {
	    next(err);
	    return;
	}
	const documentId = entity.id;
	console.log('Got document data for: ' + documentId);

	//const accessAllowed = isAccessAllowed(req, entity);
	//if (!accessAllowed) {
	//    res.redirect('/home');
	//    return;
	//}
	
	listVersions(documentId, (err, versionList) => {
            if (err) {
		console.warn('Could not get version data: ' + err);
		next(err);
		return;
            }
            res.render('user/document.pug', {
		document: entity,
		versionList: versionList,
            });
        });
    });
});

function listVersions(documentId, cb) {
    const limit = 30;
    const token = '';
    version.list(documentId, limit, token, (err, entities, hasMore) => {
	console.log(util.format('version.list() done: %s', entities));
	cb(err, entities);
    });
}


/**
 * GET /document/:id/version/<version>
 *
 * Download a document
 */
router.get('/:documentId/version/:version', (req, res, next) => {
    var documentId = req.params.documentId;
    var versionParam = req.params.version;
    console.log(util.format('Downloading document %s', documentId, versionParam));
    
    const versionNumber = parseInt(versionParam, 10);
    if (isNaN(versionNumber)) {
	req.redirect('/home');
	return;
    }

    document.read(documentId, (err, entity) => {
	if (err) {
	    next(err);
	    return;
	}
	const documentId = entity.id;
	const documentGroup = entity.group;
	
	console.log(util.format('Got document %s in group %s',
				documentId, documentGroup));
	
	const accessAllowed = isAccessAllowed(req, entity);
	if (!accessAllowed) {
	    console.log('');
	    res.redirect('/home');
	    return;
	}
	
	download(req, res, next, documentId, versionNumber);
	
	logDownloadAction(req, entity, (err) => {
	    if (err) {
		console.warn('Failed to log download action: ' + err);
	    }
	});
    });
});

function isAccessAllowed(req, document) {
    const documentId = document.id;
    const userId = req.user.id;
    
    const documentGroup = document.group;
    const userGroup = req.user.group;

    if (documentGroup !== userGroup) {
	const m = util.format(
	    'Access attempt accross group for '
		+ 'document %s (group %s) by user %s (group %s)',
	    documentId, documentGroup, userId, userGroup);
	console.log(m);
	return false;
    }
    var remoteIp = getRemoteIp(req);

    //const ipRange = '192.168.1.134/26';
    const ipRange = '2001:db8:abcd:0012:0000:0000:0000:0000/120';
    const ipInRange = ipLib.cidrSubnet(ipRange).contains(remoteIp)
    //const ipInRange = ipaddrLib.cidrSubnet(ipRange).contains(remoteIp)
    if(!ipInRange) {
	console.log(util.format('Remote IP "%s" not in range "%s"',
				remoteIp, ipRange));
	return false;
    }

    return true;
}

function getRemoteIp(req) {
    const forwardedFor = req.header('x-forwarded-for');

    var remoteIp;
    if (typeof forwardedFor !== 'undefined') {
	console.log('X-Forwarded-for is set to: ' + forwardedFor);
	// X-Forwarded-for is set to: 95.175.104.88, 172.217.21.180
	remoteIp = parseForwardedFor(forwardedFor);
    } else {
	remoteIp = req.connection.remoteAddress
    }
    return remoteIp;
}

function parseForwardedFor(forwardedFor) {
    const ipArray = forwardedFor.split(',');
    const ip = '';
    if (ipArray.length > 0) {
	const ip = ipArray[0];
    }
    return ip;
}


/**
 * GET /document/<id>/file
 *
 * Download a document
 */
router.get('/:documentId/file', (req, res, next) => {
    var documentId = req.params.documentId;
    console.log(util.format('Downloading document %s', documentId));
    
    document.read(documentId, (err, entity) => {
	if (err) {
	    next(err);
	    return;
	}
	const documentId = entity.id;
	const documentGroup = entity.group;
	const currentVersion = entity.currentVersion;
	
	console.log(util.format('Got document %s in group %s, version: %s',
				documentId, documentGroup, currentVersion));

	const accessAllowed = isAccessAllowed(req, entity);
	if (!accessAllowed) {
	    res.redirect('/home');
	    return;
	}
	
	download(req, res, next, documentId, currentVersion);

	logDownloadAction(req, entity, (err) => {
	    if (err) {
		console.warn('Failed to log download action: ' + err);
	    }
	});
    });
});

function logDownloadAction(req, document, cb) {
    const actionData = {};
    actionData.type = 'ACTION';
    actionData.action = 'Download';
    actionData.documentId = document.id,
    actionData.documentGroup = document.group,
    actionData.timestamp = (new Date()).toISOString();
    actionData.userId = req.user.id;
    actionData.userGroup = req.user.group;
    
    action.create(actionData, cb);
}

function logUploadAction(req, document, cb) {
    const actionData = {};
    actionData.type = 'ACTION';
    actionData.action = 'Upload';
    actionData.documentId = document.id,
    actionData.documentGroup = document.group,
    actionData.timestamp = (new Date()).toISOString();
    actionData.userId = req.user.id;
    actionData.userGroup = req.user.group;
    
    action.create(actionData, cb);
}

function logVersionAction(req, document, cb) {
    const actionData = {};
    actionData.type = 'ACTION';
    actionData.action = 'Version';
    actionData.documentId = document.id,
    actionData.documentGroup = document.group,
    actionData.timestamp = (new Date()).toISOString();
    actionData.userId = req.user.id;
    actionData.userGroup = req.user.group;
    
    action.create(actionData, cb);
}

function download(req, res, next, documentId, versionNumber) {
    version.find(documentId, versionNumber, (err, entities, hasMore) => {
	if (err || entities.length === 0) {
	    console.log('Could not find version: ' + err);
	    next(err);
	    return;
	}
	console.log('Got version data: ' + util.inspect(entities));
	const entity = entities[0];
	const gcsName = entity.object;
	
	//const gcsname = Date.now() + req.file.originalname;
	const file = bucket.file(gcsName);
	
	console.log('Download from: ' + gcsName);
	file.createReadStream()
	    .on('error', function(err) {
		console.warn('Error downloading file: ' +err);
		next(err);
		return;
	    })
	    .on('response', function(response) {
		// Server connected: status and headers.
		console.log('Connected, response: ' +
			    util.inspect(response.headers));
		const contentType = response.headers['content-type'];
		const fileName = gcsName.substr(13);
		if (typeof contentType === 'undefined') {
		    // No content type, download
		    const disposition = util.format('attachment; filename="%s"',
						    fileName);
		    res.setHeader('Content-Disposition', disposition);
		} else {
		    // Content found, show in the browser
		    const disposition = util.format('filename="%s"', fileName);
		    res.setHeader('content-type', 'image/png');
		    res.setHeader('Content-Disposition', disposition);
		}
	    })
	    .on('end', function() {
		// The file is fully downloaded.
		console.log('Fully downloaded ' + gcsName);
	    })
	    .pipe(res);
    });
}

/**
 * GET /books/:id/delete
 *
 * Delete a book.
 */
router.get('/:book/delete', (req, res, next) => {
    document.delete(req.params.book, (err) => {
	if (err) {
	    next(err);
	    return;
	}
	res.redirect(req.baseUrl);
    });
});

/**
 * Errors on "/document/*" routes.
 */
router.use((err, req, res, next) => {
    // Format error and forward to generic error handler for logging and
    // responding to the request
    err.response = err.message;
    next(err);
});

module.exports = router;
