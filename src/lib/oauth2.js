'use strict';

const util = require('util');
const express = require('express');

const config = require('../config');
const userData = require('../db/user.js');
const action = require('../db/action');

// [START setup]
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;

function extractProfile (profile) {
    //console.log('extractProfile()');
    //console.log(profile);

    const id = profile.id;
    const displayName = profile.displayName;
    let email = '';
    if (profile.emails && profile.emails.length) {
	email = profile.emails[0].value
    }
    
    let imageUrl = '';
    if (profile.photos && profile.photos.length) {
	imageUrl = profile.photos[0].value;
    }

    return {
	externalId: profile.id,
	displayName: profile.displayName,
	email: email,
	image: imageUrl,
    };
}

// Configure the Google strategy for use by Passport.js.
//
// OAuth 2-based strategies require a `verify` function which receives the
// credential (`accessToken`) for accessing the Google API on the user's behalf,
// along with the user's profile. The function must invoke `cb` with a user
// object, which will be set at `req.user` in route handlers after
// authentication.
passport.use(new GoogleStrategy({
    clientID: config.get('OAUTH2_CLIENT_ID'),
    clientSecret: config.get('OAUTH2_CLIENT_SECRET'),
    callbackURL: config.get('OAUTH2_CALLBACK'),
    accessType: 'offline'
}, (accessToken, refreshToken, profile, cb) => {
    // Extract the minimal profile information we need from the profile object
    // provided by Google
    const externalProfile = extractProfile(profile);

    // Find our own data from datastore before continuing
    loadFullProfile(externalProfile, (err, fullProfile) => {
	console.log('Done loading profile for email: ' + externalProfile.email);
	if (err) {
	    console.warn('Could not load profile');
	    cb(err, null);
	} else {
	    cb(null, fullProfile);
	}
    });
}));

passport.serializeUser((user, cb) => {
    cb(null, user);
});
passport.deserializeUser((obj, cb) => {
    cb(null, obj);
});

/**
 * Load application profile to extend the external profile.
 * Call cb(fullProfile)
 */
function loadFullProfile(externalProfile, cb) {
    const externalId = externalProfile.externalId;

    /*
    const groupLimit = 10;
    const groupToken = '';
    groupData.list(groupLimit, groupToken, (err, groups, hasMore) => {
	if (err) {
	    cb(err, null);
	    return;
	}
    });
    */
    
    userData.find(externalId, (err, entities, hasMore) => {
	console.log('Did search for profile. Error: ' + err);
	console.log(entities);
	if (err) {
	    cb(err, null);
	    return;
	}
	const fullProfile = JSON.parse(JSON.stringify(externalProfile));

	if (entities.length > 0) {
	    console.log('Found existing data for externalId: '
			+ externalId);
	    const entity = entities[0];
	    fullProfile.id = entity.id
	    fullProfile.group = entity.group,
	    fullProfile.role = entity.role, // Should verify...?
	    fullProfile.isAdmin = fullProfile.role === 'ADMIN';
	    //fullProfile. = entity.
	    //fullProfile. = entity.
	    
	    // Data found, end the cycle
	    cb(null, fullProfile);
	} else {
	    // No data. Create a new user
	    console.log('No existing data. New profile for externalId: '
			+ externalId);
	    fullProfile.group = 1; // Group 1 to start with...
	    fullProfile.role = 'USER';
	    fullProfile.created = (new Date()).toISOString();
	    
	    userData.create(fullProfile, (err, savedData) => {
		if (err) {
		    console.log('Could not save new profile');
		    cb(err, null);
		    return;
		}
		fullProfile.id = savedData.id;
		fullProfile.isAdmin = fullProfile.role === 'ADMIN';
		cb(null, fullProfile);
	    });
	}
    });
}


// [END setup]

const router = express.Router();


var publicUrlList = [
    '/landing',
];

// [START middleware]
// Middleware that requires the user to be logged in. If the user is not logged
// in, it will redirect the user to authorize the application and then return
// them to the original URL they requested.
function authRequired (req, res, next) {
    var url = req.originalUrl;
    var isPublic = publicUrlList.indexOf(url) >= 0;

    //console.log(util.format('%s %s', url, isPublic));
    if (!req.user && !isPublic) {
	req.session.oauth2return = req.originalUrl;

	// Show a landing page instead of login.
	// Landing page has a login action available
	//return res.redirect('/auth/login');
	return res.redirect('/landing');
    }
    next();
}

// Middleware that exposes the user's profile as well as login/logout URLs to
// any templates. These are available as `profile`, `login`, and `logout`.
function addTemplateVariables (req, res, next) {
    res.locals.profile = req.user;
    //console.log(req.user);
    res.locals.login = `/auth/login?return=${encodeURIComponent(req.originalUrl)}`;
    res.locals.logout = `/auth/logout?return=${encodeURIComponent(req.originalUrl)}`;
    next();
}
// [END middleware]

// Begins the authorization flow. The user will be redirected to Google where
// they can authorize the application to have access to their basic profile
// information. Upon approval the user is redirected to `/auth/google/callback`.
// If the `return` query parameter is specified when sending a user to this URL
// then they will be redirected to that URL when the flow is finished.
// [START authorize]
router.get(
    // Login url
    '/auth/login',

    // Save the url of the user's current page so the app can redirect back to
    // it after authorization
    (req, res, next) => {
	if (req.query.return) {
	    req.session.oauth2return = req.query.return;
	}
	next();
    },

    // Start OAuth 2 flow using Passport.js
    passport.authenticate('google', { scope: ['email', 'profile'] })
);
// [END authorize]

// [START callback]
router.get(
    // OAuth 2 callback url. Use this url to configure your OAuth client in the
    // Google Developers console
    '/auth/google/callback',

    // Finish OAuth 2 flow using Passport.js
    passport.authenticate('google'),

    // Redirect back to the original page, if any
    (req, res) => {
	const redirect = req.session.oauth2return || '/home';
	delete req.session.oauth2return;
	
	logLoginAction(req, (err) => {
	    if (err) {
		console.warn('Failed to log login action: ' + err);
	    }
	});
	res.redirect(redirect);
    }
);
// [END callback]


function logLoginAction(req, cb) {
    const actionData = {};
    actionData.type = 'AUTH';
    actionData.action = 'Login';
    actionData.timestamp = (new Date()).toISOString();
    actionData.userId = req.user.id;
    actionData.userGroup = req.user.group;
    actionData.ip = getRemoteIp(req);
    actionData.agent = req.get('User-Agent');
    
    action.create(actionData, cb);
}

function getRemoteIp(req) {
    const forwardedFor = req.header('x-forwarded-for');

    var remoteIp;
    if (typeof forwardedFor !== 'undefined') {
	console.log('X-Forwarded-for is set to: ' + forwardedFor);
	// X-Forwarded-for is set to: 95.175.104.88, 172.217.21.180
	remoteIp = parseForwardedFor(forwardedFor);
    } else {
	remoteIp = req.connection.remoteAddress
    }
    return remoteIp;
}

function logLogoutAction(req, cb) {
    const actionData = {};
    actionData.type = 'AUTH';
    actionData.action = 'Logout';
    actionData.timestamp = (new Date()).toISOString();
    actionData.userId = req.user.id;
    actionData.userGroup = req.user.group;
    actionData.ip = getRemoteIp(req);
    actionData.agent = req.get('User-Agent');
    action.create(actionData, cb);
}

// Deletes the user's credentials and profile from the session.
// This does not revoke any active tokens.
router.get('/auth/logout', (req, res) => {
    req.logout();
    
    logLogoutAction(req, (err) => {
	if (err) {
	    console.warn('Failed to log logout action: ' + err);
	}
    });
    
    res.redirect('/landing');
});

// Always require auth
router.use(authRequired);
router.use(addTemplateVariables);

module.exports = {
    extractProfile: extractProfile,
    router: router,
    required: authRequired,
    template: addTemplateVariables
};
