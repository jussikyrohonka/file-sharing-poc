'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const groupData = require('../db/group');
const userData = require('../db/user');
const actionData = require('../db/action');

const router = express.Router();

// Automatically parse request body as form data
router.use(bodyParser.urlencoded({ extended: false }));

router.use(adminRequired);

function adminRequired(req, res, next) {
    //console.log('adminRequired()');
    
    if (!req.user || !req.user.isAdmin) {
	// Not an admin user, log unauthorised attempt
	var ip = req.connection.remoteAddress;
	// or req.headers['x-forwarded-for']
	console.log('Uauthorised access attempt: '+ ip);
	return res.redirect('/'); // root or /admin?
    }
    next();
}

// Set Content-Type for all responses for these routes
router.use((req, res, next) => {
  res.set('Content-Type', 'text/html');
  next();
});

/**
 * GET /admin
 *
 * Display a page of books (up to ten at a time).
 */
router.get('/', (req, res, next) => {
    //getModel().list(10, req.query.pageToken, (err, entities, cursor) => {
//	if (err) {
//	    next(err);
//	    return;
//	}
    
    var groupList = [{name: 'a'}, {name: 'b'}, {name: 'c'}];
    var userList = [{name: 'a'}, {name: 'b'}, {name: 'c'}];
    var activityList = [{name: 'a'}, {name: 'b'}, {name: 'c'}];
    
    console.log('Start group search');
    listGroups((err, groups, hasMore) => {
	if (err) {
	    console.log('Could not find groups: ' + err);
	    next(err);
	    return;
	}
	listUsers((err, users, hasMore) => {
	    if (err) {
		console.log('Could not find users: ' + err);
		next(err);
		return;
	    }
	    listActions((err, actions, hasMore) => {
		if (err) {
		    console.log('Could not find actions: ' + err);
		    next(err);
		    return;
		}
		
		listAuthActions((err, authActions, hasMore) => {
		    if (err) {
			console.log('Could not find auth actions: ' + err);
			next(err);
			return;
		    }
		    const groupMap = makeGroupMap(groups);
		    const userMap = makeUserMap(users);
		    res.render('admin/index.pug', {
			groupList: groups,
			groupMap: groupMap,
			userMap: userMap,
			userList: users,
			actionList: actions,
			authActionList: authActions
		    });
		});
	    });
	});
    });
});

function listGroups(cb) {
    console.log('List groups');
    const limit = 20;
    const token = '';
    groupData.list(limit, token, (err, entities, hasMore) => {
	console.log('Got group list: ' + entities);
	cb(err, entities, hasMore);
    });
}

function listUsers(cb) {
    var group;
    const limit = 20;
    const token = '';
    userData.list(group, limit, token, (err, entities, hasMore) => {
	console.log('Got user list: ' + entities);
	cb(err, entities, hasMore);
    });
}

function listActions(cb) {
    const limit = 30;
    const token = '';
    actionData.listAll(limit, token, (err, entities, hasMore) => {
	console.log('Got action list: ' + entities.length);
	cb(err, entities, hasMore);
    });
}

function listAuthActions(cb) {
    const limit = 30;
    const token = '';
    actionData.listAuth(limit, token, (err, entities, hasMore) => {
	console.log('Got action list: ' + entities.length);
	cb(err, entities, hasMore);
    });
}


function makeGroupMap(groupList) {
    const map = {};
    groupList.forEach((group) => {
	map[group.id] = group.name;
    });
    console.log('Made group map: ');
    console.log(map);
    return map;
}

function makeUserMap(userList) {
    const map = {};
    userList.forEach((user) => {
	map[user.id] = user.email;
    });
    console.log('Made user map: ');
    console.log(map);
    return map;
}
/**
 * POST /admin/assignuser
 * Assign a user to a group
 */
// [START add_post]
router.get('/assignuser', (req, res, next) => {
    res.redirect('/admin');
});
router.post('/assignuser', (req, res, next) => {
    const data = req.body;
    //console.log('Assign: ');
    //console.log(data);
    //console.log(req.params);
    const userid = data.userid;
    const newGroup = data.group;

    console.log('Assigning user ' + userid + ' to group ' + newGroup);
    userData.read(userid, (err, entity) => {
	console.log('Did search for profile. Error: ' + err);
	if (err) {
	    next(err);
	    return;
	}
	// Assign new group
	entity.group = newGroup;
	userData.update(userid, entity, (err, savedData) => {
	    if (err) {
		next(err)
		return;
	    }
	    // Redirect after post
	    res.redirect('/admin');
	});
    });
});

router.post('/assignip', (req, res, next) => {
    const data = req.body;
    console.log('Assign IP: ');
    console.log(data);
    console.log(req.params);
    const groupId = data.groupid;
    const newRange = data.iprange;

    console.log('Assigning iprange ' + newRange + ' to group ' + groupId);
    groupData.read(groupId, (err, entity) => {
	console.log('Did search for Group. Error: ' + err);
	if (err) {
	    next(err);
	    return;
	}
	// Assign new range
	entity.iprange = newRange;
	groupData.update(groupId, entity, (err, savedData) => {
	    if (err) {
		next(err)
		return;
	    }
	    // Redirect after post
	    res.redirect('/admin');
	});
    });
});

// [END add_post]


/**
 * GET /books/add
 *
 * Display a form for creating a book.
 */
// [START add_get]
router.get('/add', (req, res) => {
  res.render('books/form.pug', {
    book: {},
    action: 'Add'
  });
});
// [END add_get]

/**
 * POST /books/add
 *
 * Create a book.
 */
// [START add_post]
router.post('/add', (req, res, next) => {
  const data = req.body;

  // Save the data to the database.
  getModel().create(data, (err, savedData) => {
    if (err) {
      next(err);
      return;
    }
    res.redirect(`${req.baseUrl}/${savedData.id}`);
  });
});
// [END add_post]


module.exports = router;
