'use strict';

const Datastore = require('@google-cloud/datastore');
const config = require('../config.js');

var datastore = require('./datastore.js');
var util = require('util');

const document = require('./document');
const version = require('./version');
const group = require('./group');
const user = require('./user');
const action = require('./action');

const ds = Datastore({
  projectId: config.get('GCLOUD_PROJECT')
});

run();


function run() {
    //insert();
    //createDocument();
    //createVersion();
    //createUser();
    //createGroup();
    //listDocuments();
    //listGroupDocuments();
    //listVersions();
    //listGroups();
    //listGroupUser();
    //listUsers();
    //updateDocument();
    //listGroupDocuments();
    //readUser();
    //findVersion();
    listAction();
}

function createDocument() {
    const f = 'Create document';
    
    const data = {
	name: 'name',
	group: 1,
	current_version: 1,
	uploader: 'userX',
	created: new Date(),
    };
    document.create(data, (err, result) => {
	console.log(util.format('%s: %s, %s', f, err, util.inspect(result)));
    });
}

function createVersion() {
    const f = 'Create Version';
    
    const data = {
	version: 1,
	document: 1,
	object: 'abcd-efg-hij-klm',
	created: new Date(),
    };
    version.create(data, (err, result) => {
	console.log(util.format('%s: %s, %s', f, err, util.inspect(result)));
    });
}

function createUser() {
    const f = 'Create User';
    
    const data = {
	name: 'name',
	role: 'user',
	group: 1,
	created: new Date(),
    };
    user.create(data, (err, result) => {
	console.log(util.format('%s: %s, %s', f, err, util.inspect(result)));
    });
}

function createGroup() {
    const f = 'Create Group';

    const data = {
	name: 'name',
	created: new Date(),
    };
    group.create(data, (err, result) => {
	console.log(util.format('%s: %s, %s', f, err, util.inspect(result)));
    });
}
    
function listDocuments() {
}
function listGroupDocuments() {
    var f = 'List group document';
    var group = 2;
    var limit = 10; 
    document.list(group, limit, '', (err, result) => {
	console.log(util.format('%s: %s, %s', f, err, util.inspect(result)));
    });
}
function listVersions() {
    const documentId = '4836609111359488';
    //const documentId = 4836609111359488;
    //var documentId;
    const limit = 30;
    const token = '';
    version.list(documentId, limit, token, (err, entities, hasMore) => {
	console.log(util.format('version.list() done: %s', entities));
	console.log(entities);
    });
}
function listGroups() {
    var f = 'List groups';
    var limit = 10; 
    group.list(limit, '', (err, result) => {
	console.log(util.format('%s: %s, %s', f, err, util.inspect(result)));
    });
}
function listGroupUsers() {
    var f = 'List group users';
    var limit = 10; 
    user.list(limit, '', (err, result) => {
	console.log(util.format('%s: %s, %s', f, err, util.inspect(result)));
    });
}
function listUsers() {
    var f = 'List users';
    var limit = 10; 
    user.list(limit, '', (err, result) => {
	console.log(util.format('%s: %s, %s', f, err, util.inspect(result)));
    });
}
function listAction() {
    var f = 'List actions';
    var limit = 10; 
    action.listAuth(limit, '', (err, result) => {
	console.log(util.format('%s: %s, %s', f, err, util.inspect(result)));
    });
}

function updateDocument() {
    var f = 'Update document';
    var id = 5715999101812736;
    var limit = 10;
    var data = {
	name: 'name2'
    };
    document.update(id, data, (err, result) => {
	console.log(util.format('%s: %s, %s', f, err, util.inspect(result)));
    });
}
function updateUser() {
}
function updateGroup() {
}

function readUser() {
    var f = 'Read user';
    var limit = 10; 
    user.read('5672749318012928', (err, result) => {
	console.log(util.format('%s: %s, %s', f, err, util.inspect(result)));
    });
}

function findVersion() {
    const documentId = '5724160613416960';
    const versionNumber = 1;
    version.find(documentId, versionNumber, (err, entities, hasMore) => {
	if (err || entities.length === 0) {
	    console.log('Could not find version: ' + err);
	    return;
	}
	console.log('Got version data: ' + util.inspect(entities));
    });
}

function insert() {
    var data = {
	name: 'File name 2',
	created: 'Fri Feb 23 2018 23:44:28 GMT+0200 (EET)'};
    datastore.create(data, (err, result) => {
	if (err) {
	    console.warn('Error calling create()');
	    console.warn(err);
	    return;
	}
	
	console.log('Result:');
	console.log(result);
    });
}

function list() {
    datastore.list(limit, token, (err, entities, cursor) => {
	if (err) {
	    console.warn('Error calling list()');
	    console.warn(err);
	    return;
	}
	
	console.log('Entities:');
	console.log(entities);
	console.log('Cursor:');
	console.log(cursor);
    });
}


