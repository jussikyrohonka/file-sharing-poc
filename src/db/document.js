'use strict';

const Datastore = require('@google-cloud/datastore');
const config = require('../config.js');
const format = require('./datastore-format');
const datastore = require('./datastore');

// [START config]
const ds = Datastore({
    projectId: config.get('GCLOUD_PROJECT')
});
const kind = 'Document';
// [END config]


// The ``limit`` argument determines the maximum amount of results to
// return per page. The ``token`` argument allows requesting additional
// pages. The callback is invoked with ``(err, items, nextPageToken)``.
// [START list]
function list (group, limit, token, cb) {
    console.log('Listing with group ' + group);
    const q = ds.createQuery([kind])
	  .limit(limit)
    // .order('title');
    if (typeof group !== 'undefined') {
	q.filter('group', '=', group);
    }
    q.start(token);

    datastore.list(ds, q, cb);
}
// [END list]

// Creates a new Document or updates an existing with new data. The provided
// data is automatically translated into Datastore format. The book will be
// queued for background processing.
// [START update]
function update (id, data, cb) {
    let key;
    if (id) {
	key = ds.key([kind, parseInt(id, 10)]);
    } else {
	key = ds.key(kind);
    }
    
    const entity = {
	key: key,
	data: format.toDatastore(data, ['description']) // Enumerate non-indexed properies
    };
    
    datastore.update(ds, data, entity, cb);
}
// [END update]

function create (data, cb) {
    update(null, data, cb);
}

function read (id, cb) {
    datastore.read(ds, kind, id, cb);
}

function _delete (id, cb) {
    datastore.delete(ds, kind, id, cb);
}

// [START exports]
module.exports = {
    create,
    read,
    update,
    delete: _delete,
    list
};
// [END exports]
