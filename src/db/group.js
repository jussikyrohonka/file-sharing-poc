'use strict';

const Datastore = require('@google-cloud/datastore');
const config = require('../config.js');
const format = require('./datastore-format');
const datastore = require('./datastore');


// [START config]
const ds = Datastore({
  projectId: config.get('GCLOUD_PROJECT')
});
const kind = 'Group';
// [END config]

// [START list]
function list (limit, token, cb) {
    const q = ds.createQuery([kind])
	  .limit(limit)
    //.order('title')
	  .start(token);
    
    datastore.list(ds, q, cb);
}
// [END list]

// [START update]
function update (id, data, cb) {
    let key;
    if (id) {
	key = ds.key([kind, parseInt(id, 10)]);
    } else {
	key = ds.key(kind);
    }

    const entity = {
	key: key,
	data: format.toDatastore(data, ['description'])
    };

    datastore.update(ds, data, entity, cb);
}
// [END update]

function create (data, cb) {
  update(null, data, cb);
}

function read (id, cb) {
    datastore.read(ds, kind, id, cb);
}

function _delete (id, cb) {
    datastore.delete(ds, kind, id, cb);
}

// [START exports]
module.exports = {
  create,
  read,
  update,
  delete: _delete,
  list
};
// [END exports]
