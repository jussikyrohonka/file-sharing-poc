(setq indent-tabs-mode nil)
(setq tab-width 4)
;;(infer-indentation-style)

(defun my-custom-settings-fn ()
  (setq indent-tabs-mode nil)
  (setq tab-stop-list (number-sequence 2 200 2))
  (setq tab-width 2)
  (setq indent-line-function 'insert-tab))

(add-hook 'text-mode-hook 'my-custom-settings-fn)
