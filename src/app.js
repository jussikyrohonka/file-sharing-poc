'use strict';

const path = require('path');
const util = require('util');
const express = require('express');
const session = require('express-session');
const MemcachedStore = require('connect-memcached')(session);
const passport = require('passport');
const config = require('./config');

const app = express();

// Print out what kind of a config we are running
//console.log(config.get('SECRET'));
//console.log(config.get('CLIENT_SECRET'));
//console.log(config.get('CLIENT_ID'));

app.disable('etag');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('trust proxy', true);

// [START session]
// Configure the session and session storage.
const sessionConfig = {
  resave: false,
  saveUninitialized: false,
  secret: config.get('SECRET'),
  signed: true
};

//console.log(util.format(': %s', config.get('')));
//console.log(util.format('MEMCACHE_URL: %s', process.env.MEMCACHE_URL));
//console.log(util.format('process.env.GAE_MEMCACHE_HOST: %s',
//			process.env.GAE_MEMCACHE_HOST));
//console.log(util.format('process.env.GAE_MEMCACHE_PORT: %s',
//			process.env.GAE_MEMCACHE_PORT));

//console.log(util.format('NODE_ENV: %s', config.get('NODE_ENV')));
//console.log(util.format('MEMCACHE_URL: %s', config.get('MEMCACHE_URL')));


// In production use the App Engine Memcache instance to store session data,
// otherwise fallback to the default MemoryStore in development.
if (config.get('NODE_ENV') === 'production' && config.get('MEMCACHE_URL')) {
  //sessionConfig.store = new MemcachedStore({
  //  hosts: [config.get('MEMCACHE_URL')]
  //});
}

app.use(session(sessionConfig));
// [END session]

// Static content
app.use('/static', express.static(path.join(__dirname, 'static')))

// OAuth2
app.use(passport.initialize());
app.use(passport.session());
app.use(require('./lib/oauth2').router);

// Admin section
app.use('/admin', require('./admin/admin'));

// Home section
app.use('/home', require('./homepage'));

// Document section
app.use('/document', require('./documentForm'));

app.get('/', (req, res) => {
    // Default to 'home' section
    res.redirect('/home');
});

/**
 * The landing page is accessible publicly and allows for login
 */
app.get('/landing', (req, res) => {
    console.log('In /landing');
    if (req.user) {
	console.log('Profile found, go /home');
	res.redirect('/home');
    } else {
	console.log('No profile, show landing');
	res.render('landing.pug');
    }
});

// Basic 404 handler
app.use((req, res) => {
  res.status(404).send('Not Found');
});


// Basic error handler
app.use((err, req, res, next) => {
  /* jshint unused:false */
  console.error(err);
  // If our routes specified a specific response, then send that. Otherwise,
  // send a generic message so as not to leak anything.
  res.status(500).send(err.response || 'Something broke!');
});


// "When a file is run directly from Node.js, require.main is set
// to its module. That means that it is possible to determine whether
// a file has been run directly by testing require.main === module"
if (module === require.main) {
  // Running from the command line -> Start the server
  const server = app.listen(config.get('PORT'), () => {
    const port = server.address().port;
    console.log(`App listening on port ${port}`);
  });
}

module.exports = app;
